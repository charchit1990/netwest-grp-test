# Netwest Test

HOST: localhost
PORT: 8081

# Configurations

1. Java: Java 11
2. Database: H2
3. Packaging: war

## Service URL

# Add Account: 
  1. URL: http://localhost:8081/api/account/v1/add
  2. Request: {
	"accountNumber": 76982090,
	"ownerName": "Freddie",
	"balance": 9000.0 }
  3. POST Service Type	
  4. Unit test & Integration test added	
  
# Transfer Service: 
  1. URL: http://localhost:8081/api/transaction/v1/transferService
  2. Request: {
	"sourceAccount": "73084635",
	"destinationAccount": "21956204",
	"amount": "110.0"}
  3. POST Service Type			
  4. Unit test & Integration test added		

## Running locally
```
./mvn clean install
```


## Testing
1. Import the Postman collection file into the application or copy the request body from there

### Extensions
1. Use of persisted database
2. Use of asynchronous programming backed by message queue for transactions
3. Others mentioned throughout the code
4. By default it will load some data in both table when application load. 

