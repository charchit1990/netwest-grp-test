package com.netwest.test.integration;
import static org.junit.jupiter.api.Assertions.assertEquals;
import java.math.BigDecimal;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import com.netwest.test.controller.AccountController;
import com.netwest.test.utils.AccountInput;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "local")
public class AccountAddIntegrationTest {

	@Autowired
	private AccountController accountRestController;

	@Test
	void givenAccountDetails_whenAccountAdd_thenVerifyAccountAddIsProcessed() throws Exception {
		// given
		AccountInput input = new AccountInput();
		input.setAccountNumber("73284635");
		input.setOwnerName("Tom");
		input.setBalance(new BigDecimal(27.5));

		// when
		ResponseEntity<?> responseEntity = accountRestController.addOrUpdateAccount(input);

		// then
		assertEquals(201, responseEntity.getStatusCodeValue());
	}
}
