package com.netwest.test.integration;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.math.BigDecimal;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;

import com.netwest.test.controller.TransactionController;
import com.netwest.test.utils.TransactionInput;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@ActiveProfiles(value = "local")
class MakeTransferIntegrationTest {

	@Autowired
	private TransactionController transactionRestController;

	@Test
	void givenTransactionDetails_whenMakeTransaction_thenVerifyTransactionIsProcessed() {
		// given
		TransactionInput input = new TransactionInput();
		input.setSourceAccount("73084635");
		input.setDestinationAccount("21956204");
		input.setAmount(new BigDecimal(27.5));

		// when
		ResponseEntity<?> responseEntity = transactionRestController.transferServiceAPI(input);

		// then
		assertEquals(200, responseEntity.getStatusCodeValue());
	}
}
