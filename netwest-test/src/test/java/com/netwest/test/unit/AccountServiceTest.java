package com.netwest.test.unit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import com.netwest.test.model.Account;
import com.netwest.test.repository.AccountRepository;
import com.netwest.test.repository.TransactionRepository;
import com.netwest.test.service.AccountService;
import com.netwest.test.service.AccountServiceImpl;
import com.netwest.test.service.TransactionService;
import com.netwest.test.utils.AccountInput;
import com.netwest.test.utils.TransactionInput;

import net.bytebuddy.NamingStrategy.SuffixingRandom.BaseNameResolver.ForGivenType;

@ExtendWith(SpringExtension.class)
public class AccountServiceTest {

	@TestConfiguration
	static class TransactionServiceTestContextConfiguration {

		@Bean
		public AccountServiceImpl accountService() {
			return new AccountServiceImpl();
		}
	}

	@Autowired
	private AccountService accountService;

	@MockBean
	private AccountRepository accountRepository;

	@BeforeEach
	void setUp() {
		Account account = new Account(1L, "John", "78901234", new BigDecimal(458.1));

		when(accountRepository.findByAccountNumber("78901234")).thenReturn(Optional.of(account));
	}

	@Test
	void shouldAddAccountSuccessfully() {
		final AccountInput input = new AccountInput();
		input.setAccountNumber("48573590");
		input.setBalance(new BigDecimal(50));
		input.setOwnerName("James");
		boolean isComplete = accountService.addOrUpdateAccountDetails(input);
		assertThat(isComplete).isTrue();
	}

	@Test
	void shouldThrowErrorIfAccountNumberAlredyExist() {
		var input = new AccountInput();
		input.setAccountNumber("78901234");
		input.setBalance(new BigDecimal(50));
		input.setOwnerName("John");
		boolean isComplete = accountService.addOrUpdateAccountDetails(input);
		assertThat(isComplete).isFalse();
	}

}
