package com.netwest.test.unit;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.netwest.test.controller.AccountController;
import com.netwest.test.service.AccountService;

/***
 * 
 * @author charchitkasliwal
 *
 */
@ExtendWith(SpringExtension.class)
@WebMvcTest(AccountController.class)
public class AccountRestControllerTest {

	@Autowired
	MockMvc mockMvc;

	@MockBean
	AccountService accountService;

	@Test
	void givenInvalidInput_whenaddAccount_thenVerifyBadRequest() throws Exception {
		mockMvc.perform(post("/api/account/v1/add")
	            .contentType(MediaType.APPLICATION_JSON)
	            .content("{\"ownerName\": \"David Bond\",\"accountNumber\": 7890121, \"balance\": 1200.0}"))
	            .andExpect(status().isBadRequest());
	}
	

	@Test
	void givenValidInput_whenAccountAdd_thenVerifyOk() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/account/v1/add")
				.content("{\"ownerName\": \"David Bond\",\"accountNumber\": 78901211, \"balance\": 1200.0}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isCreated());
	}
}
