package com.netwest.test.unit;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import com.netwest.test.controller.TransactionController;
import com.netwest.test.service.AccountService;
import com.netwest.test.service.TransactionService;

@ExtendWith(SpringExtension.class)
@WebMvcTest(TransactionController.class)
public class TransactionRestControllerTest {

	@Autowired
	MockMvc mockMvc;
	
	@MockBean
	TransactionService transactionService;

	@Test
	void givenMissingInput_whenMakeTransfer_thenVerifyBadRequest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/transaction/v1/transferService")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@Test
	void givenInvalidInput_whenMakeTransfer_thenVerifyBadRequest() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/transaction/v1/transferService")
				.content("{\"sourceAccount\": 258290, \"destinationAccount\": 973111, \"amount\": 1000.0}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isBadRequest());
	}

	@Test
	void givenNoAccountForInput_whenMakeTransfer_thenVerifyOk() throws Exception {
		mockMvc.perform(MockMvcRequestBuilders.post("/api/transaction/v1/transferService")
				.content("{\"sourceAccount\": 78901234, \"destinationAccount\": 48573590, \"amount\": 10.0}")
				.contentType(MediaType.APPLICATION_JSON)).andExpect(MockMvcResultMatchers.status().isOk());
	}

}
