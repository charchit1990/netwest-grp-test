package com.netwest.test.unit;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

import java.math.BigDecimal;
import java.util.Optional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.context.annotation.Bean;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.netwest.test.model.Account;
import com.netwest.test.repository.AccountRepository;
import com.netwest.test.repository.TransactionRepository;
import com.netwest.test.service.TransactionService;
import com.netwest.test.service.TransactionServiceImpl;
import com.netwest.test.utils.AccountInput;
import com.netwest.test.utils.TransactionInput;

@ExtendWith(SpringExtension.class)
class TransactionServiceTest {

    @TestConfiguration
    static class TransactionServiceTestContextConfiguration {

        @Bean
        public TransactionServiceImpl transactionService() {
            return new TransactionServiceImpl();
        }
    }

    @Autowired
    private TransactionService transactionService;

    @MockBean
    private AccountRepository accountRepository;

    @MockBean
    private TransactionRepository transactionRepository;

    @BeforeEach
    void setUp() {
        var sourceAccount = new Account(1L, "John", "78901234", new BigDecimal(458.10));
        var targetAccount = new Account(2L, "Major", "48573590", new BigDecimal(64.90));

        when(accountRepository.findByAccountNumber("78901234"))
                .thenReturn(Optional.of(sourceAccount));
        when(accountRepository.findByAccountNumber("48573590"))
                .thenReturn(Optional.of(targetAccount));
    }

    @Test
    void whenTransactionDetails_thenTransferShouldBeDenied() {
        // when
    	var sourceAccount = new AccountInput();
        sourceAccount.setAccountNumber("78901234");

        var targetAccount = new AccountInput();
        targetAccount.setAccountNumber("48573590");

        var input = new TransactionInput();
        input.setSourceAccount(sourceAccount.getAccountNumber());
        input.setDestinationAccount(targetAccount.getAccountNumber());
        input.setAmount(new BigDecimal(50));

        // then
        boolean isComplete = transactionService.transferAmountService(input);

        assertThat(isComplete).isTrue();
    }

    @Test
    void whenTransactionDetailsAndAmountTooLarge_thenTransferShouldBeDenied() {
       // when
    	var sourceAccount = new AccountInput();
        sourceAccount.setAccountNumber("78901234");

        var targetAccount = new AccountInput();
        targetAccount.setAccountNumber("48573590");

        var input = new TransactionInput();
        input.setSourceAccount(sourceAccount.getAccountNumber());
        input.setDestinationAccount(targetAccount.getAccountNumber());
        input.setAmount(new BigDecimal(500));

        // then
        boolean isComplete = transactionService.transferAmountService(input);

        assertThat(isComplete).isFalse();
    }
}
