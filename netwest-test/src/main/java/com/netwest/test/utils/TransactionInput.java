package com.netwest.test.utils;

import java.math.BigDecimal;

import javax.validation.constraints.Min;
import javax.validation.constraints.Positive;

public class TransactionInput {
	private String sourceAccount;

	private String destinationAccount;

	@Positive(message = "Transfer amount must be positive")
	@Min(value = 1, message = "Amount must be larger than 1")
	private BigDecimal amount;

	public TransactionInput() {
	}

	public String getSourceAccount() {
		return sourceAccount;
	}

	public void setSourceAccount(String sourceAccount) {
		this.sourceAccount = sourceAccount;
	}

	public String getDestinationAccount() {
		return destinationAccount;
	}

	public void setDestinationAccount(String destinationAccount) {
		this.destinationAccount = destinationAccount;
	}

	public BigDecimal getAmount() {
		return amount;
	}

	public void setAmount(BigDecimal amount) {
		this.amount = amount;
	}

	@Override
	public String toString() {
		return "TransactionInput [sourceAccount=" + sourceAccount + ", destinationAccount=" + destinationAccount
				+ ", amount=" + amount + "]";
	}

}
