package com.netwest.test.utils;

import java.math.BigDecimal;

import javax.validation.constraints.NotBlank;

public class AccountInput {

	private Long id;
	
	@NotBlank(message = "Account number is mandatory")
	private String accountNumber;

	private String ownerName;


	private BigDecimal balance;

	public AccountInput() {
	}
	
	

	public AccountInput(@NotBlank(message = "Account number is mandatory") String accountNumber, String ownerName,
			BigDecimal balance) {
		super();
		this.accountNumber = accountNumber;
		this.ownerName = ownerName;
		this.balance = balance;
	}



	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}
	
	

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public String toString() {
		return "AccountInput [accountNumber=" + accountNumber + ", ownerName=" + ownerName + ", balance=" + balance
				+ "]";
	}

}
