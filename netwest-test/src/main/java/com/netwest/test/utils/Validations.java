package com.netwest.test.utils;

import java.util.regex.Pattern;

import com.netwest.test.model.Transaction;

public class Validations {

	private static final Pattern accountNumberPattern = Pattern.compile("^[0-9]{8}$");

	public static boolean isAccountValid(String accountInput) {
		return accountNumberPattern.matcher(accountInput).find();
	}

	public static boolean isSearchTransactionValid(TransactionInput transactionInput) {
		if (!isAccountValid(transactionInput.getSourceAccount()))
			return false;

		if (!isAccountValid(transactionInput.getDestinationAccount()))
			return false;

		if (transactionInput.getSourceAccount().equals(transactionInput.getDestinationAccount()))
			return false;

		return true;
	}
}
