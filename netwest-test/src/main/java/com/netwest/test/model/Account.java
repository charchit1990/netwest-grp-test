package com.netwest.test.model;

import java.math.BigDecimal;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

@Entity
@Table(name = "account", schema = "nwg_bank")
@SequenceGenerator(name = "account_seq", sequenceName = "account_sequence", schema = "nwg_bank", initialValue = 5)
public class Account {
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "account_seq")
	private Long id;

	private String ownerName;

	private String accountNumber;

	private BigDecimal balance;

	private transient List<Transaction> transactions;

	public Account() {
		super();
	}

	public Account(Long id, String ownerName, String accountNumber, BigDecimal balance) {
		super();
		this.id = id;
		this.ownerName = ownerName;
		this.accountNumber = accountNumber;
		this.balance = balance;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getOwnerName() {
		return ownerName;
	}

	public void setOwnerName(String ownerName) {
		this.ownerName = ownerName;
	}

	public String getAccountNumber() {
		return accountNumber;
	}

	public void setAccountNumber(String accountNumber) {
		this.accountNumber = accountNumber;
	}

	public BigDecimal getBalance() {
		return balance;
	}

	public void setBalance(BigDecimal balance) {
		this.balance = balance;
	}

	public List<Transaction> getTransactions() {
		return transactions;
	}

	public void setTransactions(List<Transaction> transactions) {
		this.transactions = transactions;
	}

	@Override
	public String toString() {
		return "Account [id=" + id + ", ownerName=" + ownerName + ", accountNumber=" + accountNumber + ", balance="
				+ balance + "]";
	}

}
