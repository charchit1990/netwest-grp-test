package com.netwest.test.service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.transaction.Transactional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.netwest.test.model.Account;
import com.netwest.test.repository.AccountRepository;
import com.netwest.test.utils.AccountInput;
import com.netwest.test.utils.Validations;

@Service
public class AccountServiceImpl implements AccountService {
	@Autowired
	AccountRepository accountRepository;

	@Override
	@Transactional
	public Boolean addOrUpdateAccountDetails(AccountInput account) {
		// Condition to check is account valid and contains 8 digit
		if (!findByAccountNumber(account.getAccountNumber()).isPresent()) {
			Account _account = new Account();
			_account.setAccountNumber(account.getAccountNumber());
			_account.setBalance(Objects.isNull(account.getBalance()) ? new BigDecimal(0.0) : account.getBalance());
			_account.setOwnerName(account.getOwnerName());
			accountRepository.save(_account);
			return true;
		}

		return false;
	}

	@Override
	public List<Account> findAllAccounts() {
		return accountRepository.findAll();
	}

	@Override
	public Optional<Account> findById(Long id) {
		return accountRepository.findById(id);
	}

	@Override
	public Optional<Account> findByAccountNumber(String accountNumber) {
		return accountRepository.findByAccountNumber(accountNumber);
	}

}
