package com.netwest.test.service;

import java.util.List;
import java.util.Optional;

import com.netwest.test.model.Account;
import com.netwest.test.utils.AccountInput;

public interface AccountService {
	
	Boolean addOrUpdateAccountDetails(AccountInput account);
	
	List<Account> findAllAccounts();
	
	Optional<Account> findById(Long id);
	
	Optional<Account> findByAccountNumber(String accountNumber);

}
