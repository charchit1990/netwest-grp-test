package com.netwest.test.service;

import java.math.BigDecimal;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netwest.test.model.Account;
import com.netwest.test.model.Transaction;
import com.netwest.test.repository.AccountRepository;
import com.netwest.test.repository.TransactionRepository;
import com.netwest.test.utils.TransactionInput;

@Service
public class TransactionServiceImpl implements TransactionService {

	@Autowired
	AccountRepository accountRepository;

	@Autowired
	TransactionRepository transactionRepository;

	/***
	 * Amount Transfer Service API.
	 */
	@Override
	@Transactional
	public Boolean transferAmountService(TransactionInput transaction) {

		Optional<Account> sourceAccountNumber = accountRepository.findByAccountNumber(transaction.getSourceAccount());

		Optional<Account> destnAccountNumber = accountRepository
				.findByAccountNumber(transaction.getDestinationAccount());

		if (sourceAccountNumber.isPresent() && destnAccountNumber.isPresent()) {
			if (isAmountAvailable(transaction.getAmount(), sourceAccountNumber.get().getBalance()) == 1) {
				sourceAccountNumber.get()
						.setBalance(sourceAccountNumber.get().getBalance().subtract(transaction.getAmount()));
				destnAccountNumber.get().setBalance(destnAccountNumber.get().getBalance().add(transaction.getAmount()));
				Transaction _transaction = new Transaction();
				_transaction.setAmount(transaction.getAmount());
				_transaction.setSourceAccountId(sourceAccountNumber.get().getId());
				_transaction.setTargetAccountId(destnAccountNumber.get().getId());
				transactionRepository.save(_transaction);
				return true;
			}
		}
		return false;
	}

	private int isAmountAvailable(BigDecimal amount, BigDecimal accountBalance) {
		return (accountBalance.subtract(amount)).compareTo(new BigDecimal(0));
	}

}
