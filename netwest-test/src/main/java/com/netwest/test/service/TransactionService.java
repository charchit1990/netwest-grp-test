package com.netwest.test.service;

import com.netwest.test.model.Transaction;
import com.netwest.test.utils.TransactionInput;

public interface TransactionService {

   Boolean	transferAmountService(TransactionInput transaction);
}
