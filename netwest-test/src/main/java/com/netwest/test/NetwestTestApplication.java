package com.netwest.test;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NetwestTestApplication {

	public static void main(String[] args) {
		SpringApplication.run(NetwestTestApplication.class, args);
	}

}
