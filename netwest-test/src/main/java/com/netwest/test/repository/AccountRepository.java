package com.netwest.test.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.netwest.test.model.Account;

@Repository
public interface AccountRepository  extends JpaRepository<Account, Long>{

	 Optional<Account> findByAccountNumber(String accountNumber);

}
