package com.netwest.test.controller;


import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netwest.test.service.TransactionService;
import com.netwest.test.utils.TransactionInput;
import com.netwest.test.utils.Validations;

/***
 * 
 * @author charchitkasliwal
 *
 */
@RestController
@RequestMapping("/api/transaction")
public class TransactionController {
	private static final Logger LOGGER = LoggerFactory.getLogger(TransactionController.class);

	private static final String INVALID_TRANSACTION = "Account information is invalid or transaction has been denied for your protection. Please try again.";

	@Autowired
	TransactionService transactionService;
	
	
	/**
	 * 
	 * @param transaction
	 * @return
	 */
	@PostMapping("/v1/transferService")
	public ResponseEntity<?> transferServiceAPI(@Valid @RequestBody TransactionInput transaction) {
		LOGGER.info("Initiate Transaction");
		if (Validations.isSearchTransactionValid(transaction)) {
			boolean isComplete = transactionService.transferAmountService(transaction);
			return new ResponseEntity<>(isComplete, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(INVALID_TRANSACTION, HttpStatus.BAD_REQUEST);
		}
		
	
	}

}
