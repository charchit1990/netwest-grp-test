package com.netwest.test.controller;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.netwest.test.service.AccountService;
import com.netwest.test.utils.AccountInput;
import com.netwest.test.utils.Validations;

/***
 * 
 * @author charchitkasliwal
 *
 */

@RestController
@RequestMapping("/api/account")
public class AccountController {
	@Autowired
	AccountService accountService;

	/***
	 * Method to add account details IN-Memory Database
	 * 
	 * @throws Exception
	 */
	@PostMapping("/v1/add")
	public ResponseEntity<?> addOrUpdateAccount(@Valid @RequestBody AccountInput account) throws Exception {
		if (Validations.isAccountValid(account.getAccountNumber())) {
			Boolean isComplete = accountService.addOrUpdateAccountDetails(account);
			return new ResponseEntity<>(isComplete, HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>("Please enter valid account number, it should be numeric 8 digit. Or Account number already exist.", 
					HttpStatus.BAD_REQUEST);
		}
	}

}
