CREATE SCHEMA nwg_bank;

CREATE SEQUENCE nwg_bank.account_sequence START WITH 5;
CREATE TABLE nwg_bank.account (
    id bigint NOT NULL PRIMARY KEY,
    account_number CHAR(8) NOT NULL,
    balance NUMERIC(10,4) NOT NULL,
    owner_name VARCHAR(50) NOT NULL,
    UNIQUE (account_number));

CREATE SEQUENCE nwg_bank.transaction_sequence START WITH 5;
CREATE TABLE nwg_bank.transaction (
    id bigint NOT NULL PRIMARY KEY,
    source_account_id bigint NOT NULL REFERENCES nwg_bank.account(id),
    target_account_id bigint NOT NULL REFERENCES nwg_bank.account(id),
    amount NUMERIC(10,4) NOT NULL);
