INSERT INTO nwg_bank.account (id, account_number, balance, owner_name)
VALUES (1, '73084635', 1071.78, 'David Bond');
INSERT INTO nwg_bank.account (id, account_number, balance, owner_name)
VALUES (2, '21956204', 67051.01, 'Keith Pristley');

INSERT INTO nwg_bank.transaction (id, source_account_id, target_account_id, amount)
VALUES (1, 1, 2, 100.00);
INSERT INTO nwg_bank.transaction (id, source_account_id, target_account_id, amount)
VALUES (2, 1, 2, 100.00);

INSERT INTO nwg_bank.transaction (id, source_account_id, target_account_id, amount)
VALUES (3, 2, 1, 10000.00);
